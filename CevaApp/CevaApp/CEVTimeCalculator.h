//
//  CEVTimeCalculator.h
//  CevaApp
//
//  Created by Thomas on 12/21/13.
//  Copyright (c) 2013 Thomas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CEVTime.h"

@interface CEVTimeCalculator : NSObject

@property (weak, nonatomic) CEVTime *totalTime;
@property (weak, nonatomic) NSNumber *bottleSize;
@property (weak, nonatomic) NSNumber *currentTemperature;
@property (weak, nonatomic) NSNumber *freezerTemperature;
@property (weak, nonatomic) NSNumber *desiredTemperature;
@end
