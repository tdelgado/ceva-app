//
//  CEVTime.h
//  CevaApp
//
//  Created by Thomas on 12/21/13.
//  Copyright (c) 2013 Thomas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CEVTime : NSObject

@property (weak, nonatomic) NSNumber *time;

@end
