//
//  CEVAppDelegate.h
//  CevaApp
//
//  Created by Thomas on 12/21/13.
//  Copyright (c) 2013 Thomas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CEVAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
