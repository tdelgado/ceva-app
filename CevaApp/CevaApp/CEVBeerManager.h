//
//  CEVBeerManager.h
//  CevaApp
//
//  Created by Thomas on 12/21/13.
//  Copyright (c) 2013 Thomas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CEVTimeCalculator.h"

@interface CEVBeerManager : NSObject

@property (weak, nonatomic) CEVTimeCalculator *calculator;
@end
