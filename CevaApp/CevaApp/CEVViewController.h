//
//  CEVViewController.h
//  CevaApp
//
//  Created by Thomas on 12/21/13.
//  Copyright (c) 2013 Thomas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CEVViewController : UIViewController <UIPickerViewDataSource,UIPickerViewDelegate>

@end
